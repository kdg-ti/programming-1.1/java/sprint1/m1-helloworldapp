package guess;

import java.util.Scanner;

public class Guess {
  public static void main(String[] args) {
    int answer = 1024;
    Scanner keyboard = new Scanner(System.in);
    System.out.print("Enter the number of bytes in a kilobyte: ");
     int  guess = keyboard.nextInt();
      if (guess < answer) {
        System.out.println(guess + " is too low...");
      }
      if (guess > answer) {
        System.out.println(guess + " is too high...");
      } // if
    if (guess == answer) {
      System.out.println("Congratulations, your must be an IT student!");
    }
    keyboard.close();
  } // main
} // class
